/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        nunito: ['"Nunito", sans-serif'],
      },
      colors: {
        primary: "#0C4237",
        secondary: "#FFAD61",
        gray: "#94a3b8",
      },
    },
  },
  plugins: [],
};

import Instagram from "../../assets/icon/Instagram";
import Twitter from "../../assets/icon/Twitter";

const Footer = () => {
  const currentYear = new Date().getFullYear();
  return (
    <footer className="footer container mx-auto px-5 md:px-3 py-2">
      <div className="footer-top text-center pt-[50px] border-b-[1px] pb-5">
        <h1 className="text-4xl font-bold">Ghiterra</h1>
        <ul className="flex items-center justify-center mt-5 gap-5">
          <li>
            <a href="">Home</a>
          </li>
          <li>
            <a href="">About</a>
          </li>
          <li>
            <a href="">Our Service</a>
          </li>
          <li>
            <a href="">Portfolio</a>
          </li>
          <li>
            <a href="">Contact</a>
          </li>
        </ul>
      </div>
      <div className="footer-bottom py-3 flex justify-between items-center">
        <p className="text-xs">
          Copyright © {currentYear}
          <span className="font-semibold"> Ghiterra</span>. All Rights Reserved
        </p>
        <ul className="flex items-center justify-center  gap-5">
          <li>
            <a href="">
              <Instagram />
            </a>
          </li>
          <li>
            <a href="">
              <Twitter />
            </a>
          </li>
        </ul>
      </div>
    </footer>
  );
};

export default Footer;

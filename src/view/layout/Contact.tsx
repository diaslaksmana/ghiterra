import Email from "../../assets/icon/Email";

const Contact = () => {
  return (
    <section id="contact">
      <div className="container contact  mx-auto px-5 md:px-3">
        <div className="box overflow-hidden relative z-10 bg-primary p-10 flex flex-col items-center rounded-3xl">
          <h1 className="font-semibold text-xl text-white">
            WHAT ARE YOU WAITING?
          </h1>
          <h1 className="font-semibold text-center text-5xl text-white my-5">
            Let’s collaborate!
          </h1>
          <p className="font-light text-lg text-gray max-w-[850px] mx-auto text-center">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation.
          </p>
          <p className="flex gap-2 my-5 text-white">
            <Email /> ghiterra@gmail.com
          </p>
          <a
            href=""
            className="border-white border-[1px] py-2 px-4 rounded-full text-white font-semibold"
          >
            GET STARTED
          </a>
        </div>
      </div>
    </section>
  );
};

export default Contact;

const Hero = () => {
  return (
    <section id="hero" className="hero container  mx-auto px-5 md:px-3">
      <div className=" grid grid-cols-1 sm:grid-cols-2 gap-4 items-center min-h-screen">
        <div className="overview ">
          <h1 className="font-bold text-6xl text-primary mb-5 max-w-[550px] leading-tight">
            Transforming Visions into <strong>Digital</strong> Reality
          </h1>
          <p className="text-base font-normal mb-10">
            Transforming Ideas into <strong>Reality with Expertise in </strong>
            Custom Software Development,
            <strong> Scalable Solutions, and Seamless Integration</strong>
          </p>
          <a
            href=""
            className="border-2 border-primary py-2 px-4 rounded-full text-primary hover:bg-primary hover:text-white"
          >
            Get in touch
          </a>
        </div>
        <div className="hero-image text-right">
          <div className="img ">
            <img
              src="https://i.ibb.co/BG9Qhyf/3d-casual-life-online-video-conference-in-office.png"
              alt="img"
              className="sm:max-w-[450px] max-w-[350px] h-[450px] mx-auto object-contain"
            />
          </div>
        </div>
      </div>
    </section>
  );
};

export default Hero;

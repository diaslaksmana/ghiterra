const About = () => {
  return (
    <section id="about" className="about relative z-10 bg-primary">
      <div className="container py-[50px] mx-auto px-5 md:px-3">
        <div className=" block sm:flex flex-row gap-32 items-center min-h-[80vh] ">
          <div className="basis-1/4 ">
            <div className="cover mb-5">
              <img
                src="https://images.pexels.com/photos/65438/pexels-photo-65438.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
                alt="img"
                className="rounded-3xl"
              />
            </div>
          </div>
          <div className="basis-3/4">
            <div className="overview">
              <h1 className="font-bold text-5xl text-white mb-5">About Us</h1>
              <p className="text-white">
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab,
                eum sequi porro corrupti vero illo aspernatur iure? Totam harum
                ea corporis modi non facere possimus excepturi culpa quisquam.
                Aspernatur, facere. Lorem ipsum dolor sit amet consectetur,
                adipisicing elit. Ab, eum sequi porro corrupti vero illo
                aspernatur iure? Totam harum ea corporis modi non facere
                possimus excepturi culpa quisquam. Aspernatur, facere. <br />
                <br />
                Lorem ipsum dolor sit amet consectetur, adipisicing elit. Ab,
                eum sequi porro corrupti vero illo aspernatur iure? Totam harum
                ea corporis modi non facere possimus excepturi culpa quisquam.
                Aspernatur, facere.Lorem ipsum dolor sit amet consectetur,
                adipisicing elit. Ab, eum sequi porro corrupti vero illo
                aspernatur iure? Totam harum ea corporis modi non facere
                possimus excepturi culpa quisquam. Aspernatur, facere.Lorem
                ipsum dolor sit amet consectetur, adipisicing elit. Ab, eum
                sequi porro corrupti vero illo aspernatur iure? Totam harum ea
                corporis modi non facere possimus excepturi culpa quisquam.
                Aspernatur, facere.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default About;

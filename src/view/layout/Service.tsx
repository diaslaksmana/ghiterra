import { DataService } from "../../constant/DataService";

const Service = () => {
  return (
    <section id="service" className="service py-[100px] relative bg-[#FFFEF3]">
      <div className="container mx-auto px-5 md:px-3 ">
        <h1 className="font-semibold text-base uppercase text-center">
          Our Solution
        </h1>
        <h1 className="font-bold text-5xl text-primary text-center mb-5">
          Discover Our Service
        </h1>
        <p className="text-center mb-5 font-light max-w-[850px] mx-auto text-gray">
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Est, minima
          praesentium iusto temporibus consequuntur necessitatibus, ipsa soluta
          impedit tempora accusantium ea quaerat. Voluptas sint ipsam neque et,
          reprehenderit expedita! Odit.
        </p>
        <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-7">
          {DataService.map((item) => (
            <div className="card relative">
              <div className="card-header">
                <img
                  src={item.image}
                  alt={item.name}
                  className="min-h-[500px] w-full max-h-[500px] object-cover rounded-xl"
                />
              </div>
              <div className="card-body absolute bottom-[15px] w-full">
                <div className="overview bg-white mx-[10px] p-2 rounded-xl">
                  <h1 className="font-bold text-xl text-primary">
                    {item.name}
                  </h1>
                  <p className="text-sm">{item.description}</p>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default Service;

import { DataPortfolio } from "../../constant/DataPortfolio";

const Portfolio = () => {
  return (
    <section id="portfolio">
      <div className="container py-[80px] mx-auto px-5 md:px-3 ">
        <div className="overview text-center">
          <h2 className="font-semibold text-base uppercase">Our Work</h2>
          <h1 className="font-bold text-5xl text-primary text-center mb-5">
            Explore Our Portfolio
          </h1>
          <p className="max-w-[800px] mx-auto text-gray">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud exercitation.
          </p>
        </div>
        <div className="grid sm:grid-cols-3 grid-cols-1 gap-10 mt-5">
          {DataPortfolio.map((item, index) => (
            <div className="card-portfolio" key={index}>
              <figcaption>
                <h3>{item.name}</h3>
              </figcaption>
              <img src={item.image} alt="img" />
              <a href="#"></a>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default Portfolio;

import { useEffect, useState } from "react";
import { DataProsses } from "../../constant/DataProsses";

const ByStep = () => {
  // State to track the active item
  const [activeItem, setActiveItem] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      // Move to the next item in a circular loop
      setActiveItem((prevIndex) => (prevIndex + 1) % DataProsses.length);
    }, 5000); // Adjust the interval as needed

    return () => clearInterval(interval);
  }, [activeItem]);

  // Function to handle item click
  const handleItemClick = (index: number) => {
    // Update the active item in state
    setActiveItem(index);
  };
  return (
    <section id="step" className="step relative z-10 bg-[#FFF6E5] py-[80px] ">
      <div className="container mx-auto px-5 md:px-3 pt-[50px]">
        <div
          className={` grid grid-cols-1 sm:grid-cols-2 items-center gap-20 ${
            activeItem === activeItem ? `active${activeItem}` : ""
          }`}
        >
          <div className="cover pr-0 sm:pr-[150px]">
            <div className="img rounded-3xl h-[600px] w-full object-cover"></div>
          </div>
          <div>
            <h1 className="font-semibold text-base uppercase">Our Workflow</h1>
            <h1 className="font-bold text-5xl text-primary">
              Discover Our Process
            </h1>
            <p className=" text-[#94a3b8] mb-[20px]">
              Lorem ipsum dolor sit, amet consectetur adipisicing elit.
              Laudantium architecto reprehenderit nulla tenetur?
            </p>
            {/* <Accordion items={accordionData} /> */}
            <ul className="list-service ">
              {DataProsses.map((item, index) => (
                <li
                  key={index}
                  onClick={() => handleItemClick(index)}
                  className={` cursor-pointer flex gap-3 mb-5 relative z-10 ${
                    index === activeItem ? "active" : ""
                  }`}
                >
                  <div className="icon bg-secondary border-[2px] border-primary h-[35px] w-[35px] rounded-full grid place-content-center text-white">
                    <span className="font-bold text-2xl ">{item.image}</span>
                  </div>
                  <div>
                    <h1 className="font-bold text-2xl">{item.name}</h1>
                    <p className="font-light desc max-w-[350px] sm:max-w-[500px] text-gray">
                      {item.description}
                    </p>
                  </div>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ByStep;

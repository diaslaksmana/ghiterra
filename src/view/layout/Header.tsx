import { useEffect } from "react";

const Header = () => {
  useEffect(() => {
    const handleScroll = () => {
      const body = document.body;
      if (window.scrollY > 0) {
        body.classList.add("scrolled");
      } else {
        body.classList.remove("scrolled");
      }
    };
    // Attach the event listener
    window.addEventListener("scroll", handleScroll);
    // Cleanup the event listener on component unmount
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []); // Empty dependency array means this effect runs once after the component mounts
  return (
    <header id="header" className="haeder">
      <nav className="container mx-auto px-5 md:px-3 py-5 flex justify-between items-center">
        <div className="nav-brand">
          <h1 className="text-4xl font-bold leading-normal">Ghiterra</h1>
        </div>
        <div className="nav-menu">
          <ul className="flex items-center gap-5">
            <li>
              <a href="#">Home</a>
            </li>
            <li>
              <a href="#about">About</a>
            </li>
            <li>
              <a href="#service">Our Service</a>
            </li>
            <li>
              <a href="#portfolio">Portfolio</a>
            </li>
            <li>
              <a
                href="#contact"
                className="py-1 font-semibold px-3 rounded-full text-white bg-primary "
              >
                Contact
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
  );
};

export default Header;

import About from "../layout/About";
import Contact from "../layout/Contact";
import Footer from "../layout/Footer";
import Header from "../layout/Header";
import Hero from "../layout/Hero";
import Portfolio from "../layout/Portfolio";
import Service from "../layout/Service";
import ByStep from "../layout/Step";

const Home = () => {
  return (
    <>
      <Header />
      <Hero />
      <About />
      <Service />
      <ByStep />
      <Portfolio />
      <Contact />
      <Footer />
    </>
  );
};

export default Home;

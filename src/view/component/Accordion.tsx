import React, { useState } from "react";

interface Accordion {
  title: string;
  children: React.ReactNode;
}
interface AccordionProps {
  items: Accordion[];
}

export const Accordion = ({ items }: AccordionProps) => {
  const [activeAccordion, setAciveAccordion] = useState(0);

  const hanldeAccordion = (index: number) => {
    setAciveAccordion((prevIndex) => (prevIndex === index ? -1 : index));
  };
  return (
    <>
      <section className="mt-5">
        <div className="accordion" id="accordionExample">
          {items.map((accordion, index) => (
            <div className="accordion-item " key={index}>
              <h2
                className={`accordion-header relative w-full text-2xl cursor-pointer  text-black font-bold ${
                  index === activeAccordion ? "open" : ""
                }`}
              >
                <button
                  className={`accordion-button  w-full text-left flex items-center justify-between ${
                    activeAccordion === index ? "active" : " "
                  }`}
                  type="button"
                  onClick={() => hanldeAccordion(index)}
                >
                  {accordion.title}
                </button>
              </h2>
              <div
                className={`accordion-collapse  ${" relative w-full py-2 px-6"} ${
                  activeAccordion === index ? "open" : ""
                }`}
              >
                <div className="accordion-body text-gray">
                  {accordion.children}
                </div>
              </div>
            </div>
          ))}
        </div>
      </section>
    </>
  );
};

export const DataPortfolio = [
  {
    id: 1,
    name: "Propert Website",
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.",
    image:
      "https://diaslaksmana.com/static/media/property.e62b420dcca1406e3aa7.png",
  },
  {
    id: 2,
    name: "Company Profile",
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.",
    image:
      "https://diaslaksmana.com/static/media/projek7.0936f00d812e3401552f.jpg",
  },
  {
    id: 3,
    name: "Company Profile",
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.",
    image:
      "https://diaslaksmana.com/static/media/technolabs.a9348168648e0150d29c.png",
  },
  {
    id: 4,
    name: "Dashboard App",
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.",
    image:
      "https://diaslaksmana.com/static/media/dashboard2.138b0a9312f7fa2ee28d.png",
  },
  {
    id: 5,
    name: "Company Profile",
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.",
    image: "https://i.ibb.co/D5KDB2J/capital.jpg",
  },
  {
    id: 6,
    name: "Dashboard App",
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.",
    image:
      "https://i.ibb.co/qBKL0hG/Whats-App-Image-2023-11-27-at-10-14-33-98c2302e.jpg",
  },
];

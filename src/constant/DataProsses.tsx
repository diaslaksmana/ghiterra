import Done from "../assets/icon/Done";
import Idea from "../assets/icon/Idea";
import Mockup from "../assets/icon/Mockup";
import Testing from "../assets/icon/Testing";
import Code from "../view/layout/Code";

export const DataProsses = [
  {
    id: 1,
    name: "Concept",
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.",
    image: <Idea />,
  },
  {
    id: 2,
    name: "Prototype",
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.",
    image: <Mockup />,
  },
  {
    id: 3,
    name: "Development",
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.",
    image: <Code />,
  },
  {
    id: 4,
    name: "Testing",
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.",
    image: <Testing />,
  },
  {
    id: 5,
    name: "Release",
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. Velit officia consequat duis enim velit mollit.",
    image: <Done />,
  },
];

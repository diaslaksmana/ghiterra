export const DataService = [
  {
    id: 1,
    name: "Web Development",
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. ",
    image:
      "https://images.pexels.com/photos/3585001/pexels-photo-3585001.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 2,
    name: "Mobile Apps",
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. ",
    image:
      "https://images.pexels.com/photos/5054349/pexels-photo-5054349.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 3,
    name: "Content Writer",
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. ",
    image:
      "https://images.pexels.com/photos/1766604/pexels-photo-1766604.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
  {
    id: 4,
    name: "UI/UX Design",
    description:
      "Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet sint. ",
    image:
      "https://images.pexels.com/photos/196644/pexels-photo-196644.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1",
  },
];
